module.exports = (grunt) ->
	grunt.initConfig
		build:
			template:
				tasks: [
					'clean'
					'bump'
					'copy'
				]

		clean:
			all: 'build'

		copy:
			src:
				expand: true
				cwd: "src"
				src: ["{,**/}*"]
				dest: "build"


			rest:
				expand: true
				src: [
					"package.json"
					"README.md"
					"server.*"
				]
				dest: "build"

		bump:
			options:
				commit: false
				createTag: false
				push: false

			all:
				commit: false
				createTag: false
				push: false

	grunt.registerMultiTask 'build', 'Build app', ->
		grunt.task.run @data.tasks

	require('load-grunt-tasks')(grunt)

	grunt.registerTask('default', ['build'])
