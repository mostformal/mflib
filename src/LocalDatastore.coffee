_ = require('lodash')

module.exports = LocalDatastore = (datastoreName, objectStores, opts = {}) ->

	createObjectStores = (datastore) ->
		availableStores = Array(datastore.objectStoreNames...)
		storesToCreate = _.difference(objectStores, availableStores)
		console.log("Creating stores: #{JSON.stringify(storesToCreate)}")
		storesCreated = 0
		for store in storesToCreate
			datastore.createObjectStore(store)

		if opts.deleteStores
			storesToDelete = _.difference(availableStores, objectStores)
			console.log("Deleting stores: #{JSON.stringify(storesToDelete)}")
			for store in storesToDelete
				datastore.deleteObjectStore(store)

	_datastore = null

	getDatastore = (version) ->
		if _datastore then return Promise.resolve(_datastore)

		new Promise (resolve, reject) ->
			if version
				req = indexedDB.open(datastoreName, version)
			else
				req = indexedDB.open(datastoreName)

			req.onupgradeneeded = (e) ->
				db = e.target.result
				createObjectStores(db)

			req.onsuccess = (e) ->
				db = e.target.result
				if objectStores
					availableStores = Array(db.objectStoreNames...)
					storesToCreate = _.difference(objectStores, availableStores)
					storesToDelete = _.difference(availableStores, objectStores)
					if storesToCreate.length or (storesToDelete.length and opts.deleteStores)
						version = db.version
						db.close()
						return resolve(getDatastore(version + 1))

				_datastore = db
				resolve(_datastore)

			req.onerror = (e) ->
				err = new Error("Error opening database: #{req.error?.message ? req.errorCode}")
				reject(err)

	request = (table, access, callback) ->
		getDatastore().then (datastore) ->
			new Promise (resolve, reject) ->
				objectStore = datastore.transaction([table], access)
					.objectStore(table)
				req = callback(objectStore)

				req.onsuccess = (e) ->
					resolve(e.target.result)

				req.onerror = (e) ->
					reject(new Error("Datastore error #{req.error?.message ? req.errorCode}"))

	# --------

	all = (table) ->
		getDatastore().then (datastore) ->
			new Promise (resolve, reject) ->
				objectStore = datastore.transaction([table], 'readonly')
					.objectStore(table)
				req = objectStore.openCursor()
				result = []
				req.onsuccess = (e) ->
					cursor = e.target.result
					if cursor
						result.push(cursor.value)
						cursor.continue()
					else
						resolve(result)

				req.onerror = (e) ->
					reject(new Error("Datastore error #{req.error?.message ? req.errorCode}"))


	_delete = (table, id) ->
		request table, 'readwrite', (objectStore) ->
			objectStore.delete(id)

	get = (table, id) ->
		request table, 'readonly', (objectStore) ->
			objectStore.get(id)

	insert = (table, data) ->
		request table, 'readwrite', (objectStore) ->
			objectStore.put(data, data.id)

	insertOrUpdate = insert

	extend = (table, data) ->
		get(table, data.id)
		.then (object) ->
			if not object then return insertOrUpdate(table, data)

			newObject = _.extend({}, object, data)

			if data.newId
				_delete(table, data.id)
				.then ->
					newObject.id = newObject.newId
					delete newObject.newId
					insertOrUpdate(table, newObject)
					return newObject
			else
				insertOrUpdate(table, newObject)
				return newObject

	clear = ->
		getDatastore()
		.then (datastore) ->
			for table in datastore.objectStoreNames
				request table, 'readwrite', (objectStore) ->
					objectStore.clear()

	serializer = (key, data) ->
		if data instanceof ArrayBuffer then return
		if key == 'dropbox' then return
		return data

	dump = ({save}) ->
		getDatastore()
		.then (datastore) ->
			Promise.all(all(table) for table in datastore.objectStoreNames when table != 'dump')
			.then (result) ->

				data = {}
				for table, idx in availableStores when table != 'dump'
					keyedResult = {}
					for item in result[idx]
						keyedResult[item.id] = item
					data[table] = keyedResult

				dump = {
					id: Date.now()
					data: JSON.stringify(data, serializer)
				}
				if save then insert('dump', dump)
				return dump

	getLatestDump = ->
		getDatastore()
		.then (datastore) ->
			all('dump')
			.then (result) ->
				if not result?.length
					_default = {}
					for table in datastore.objectStoreNames when table != 'dump'
						_default[table] = {}
					return {id: 0, data: JSON.stringify(_default)}

				result.sort((a, b) -> b.id - a.id)[0]

	{
		all
		clear
		delete: _delete
		get
		extend
		insert
		insertOrUpdate
		dump
		getLatestDump
	}
