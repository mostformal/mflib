arrayBufferFromFile = (file) ->
	###
	Reads a file and resolves with an array buffer with the file
	contents.

	Returns {Promise}
	###
	if not (file instanceof File or file instanceof Blob)
		throw new Error("File is undefined")

	new Promise (resolve, reject) ->
		fileReader = new FileReader()

		fileReader.onload = (e) ->
			resolve(@result)

		fileReader.onabort = (e) ->
			reject("Loading file #{file.name} was aborted: " + e)

		fileReader.onerror = (e) ->
			reject("Loading #{file.name} failed: " + e)

		fileReader.readAsArrayBuffer(file)

fileFromUrl = (url) ->
	###
	Create a file/blob from an url. Can be a data url or object url.
	###
	new Promise (resolve, reject) ->
		xhr = new XMLHttpRequest()
		xhr.open('GET', url, true)
		xhr.responseType = 'blob'

		xhr.onload = (e) ->
			if this.status < 400
				resolve(this.response)
			else
				reject(this.response)

		xhr.onerror = (e) ->
			reject(e)

		xhr.send()

imageFromFile = (file) ->
	imageWithUrl(window.URL.createObjectURL(file), file.type)

imageWithUrl = (url, type) ->
	new Promise (resolve, reject) ->
		image = new Image()
		image.type = type
		image.dataReady = false

		image.onload = ->
			image.dataReady = true
			resolve(image)

		image.onerror = ->
			reason = this.error or LOAD_MEDIA_FAILED
			reject(new Error("Failed to load image '#{file.name}'. #{reason}"))

		image.src = url

loadImage = (url) ->
	return new Promise (resolve, reject) ->
		window.URL = window.URL ? window.webkitURL;
		image = new Image()

		onLoad = ->
			image.dataReady = true
			if window.URL? and window.revokeObjectURL?
				window.revokeObjectURL(image.src)
			image.removeEventListener('load', onLoad)
			image.removeEventListener('error', onError)
			resolve(image)

		onError = (e) ->
			image.removeEventListener('load', onLoad)
			image.removeEventListener('error', onError)
			reject(new Error("Could not load image from #{url}, #{e}"))

		image.addEventListener('load', onLoad, false)
		image.addEventListener('error', onError, false)

		image.src = url

urlFromFile = (file) ->
	window.URL.createObjectURL(file)

module.exports = {
	arrayBufferFromFile
	fileFromUrl
	imageFromFile
	imageWithUrl
	loadImage
	urlFromFile
}
