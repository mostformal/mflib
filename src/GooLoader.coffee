ready = false

runScene = (url, parentElement, options = {}) ->
	{SystemBus} = window.goo
	gooRunner = initGoo(options)
	{autoStart} = options

	SystemBus.addListener 'goo.startGameLoop', ->
		if ready
			startGameLoop(gooRunner, options)
		else
			autoStart = true
	, true

	SystemBus.addListener 'goo.stopGameLoop', ->
		if ready
			gooRunner.stopGameLoop()
		else
			autoStart = false
	, true

	canvas = gooRunner.renderer.domElement
	canvas.tabIndex = -1
	parentElement.appendChild(canvas)
	loadScene(gooRunner, url)
	.then ->
		process(gooRunner)
		ready = true
		if autoStart != false
			startGameLoop(gooRunner, options)

initGoo = (options) ->
	{
		GooRunner
		AnimationSystem
		StateMachineSystem
		HtmlSystem
		PhysicsSystem
		ColliderSystem
		TimelineSystem
		SystemBus
	} = window.goo

	gooRunner = new GooRunner
		antialias: options.antialias ? true
		manuallyStartGameLoop: true
		useDevicePixelRatio: options.useDevicePixelRatio ? true
		logo: false
		alpha: options.alpha ? true

	if AnimationSystem? then gooRunner.world.add(new AnimationSystem())
	if StateMachineSystem? then gooRunner.world.add(new StateMachineSystem(gooRunner))
	if HtmlSystem? then gooRunner.world.add(new HtmlSystem(gooRunner.renderer))
	if PhysicsSystem? and ColliderSystem?
		gooRunner.world.add(new PhysicsSystem())
		gooRunner.world.add(new ColliderSystem())
	if TimelineSystem? then gooRunner.world.add(new TimelineSystem())

	return gooRunner


startGameLoop = (gooRunner, options) ->
	{Renderer, SystemBus} = window.goo
	gooRunner.renderer.domElement.style.display = 'block'
	gooRunner.startGameLoop()
	gooRunner.renderer.checkResize(Renderer.mainCamera)
	if options.focus != false
		gooRunner.renderer.domElement.focus()
	SystemBus.emit('goo.started')


loadScene = (gooRunner, url) ->

	{Ajax, DynamicLoader, SystemBus} = goo
	Ajax.crossOrigin = true

	loader = new DynamicLoader
		world: gooRunner.world
		rootPath: "#{url}/res"

	return loader.load('root.bundle')
	.then (bundle) ->
		scene = (config for key, config of bundle when /\.scene$/.test(key))[0]
		if not scene?.id
			throw new Error('No scene in bundle')

		loader.load scene.id,
			preloadBinaries: true
			progressCallback: (handled, total) ->
				SystemBus.emit('goo.loadingProgress', {handled, total})
		.then (result) ->
			return result

process = (gooRunner) ->
	###
	Do initial processing before we start the scene
	###

	world = gooRunner.world
	transformSystem = world.getSystem('TransformSystem')
	cameraSystem = world.getSystem('CameraSystem')
	boundingSystem = world.getSystem('BoundingUpdateSystem')
	lightingSystem = world.getSystem('LightingSystem')
	renderSystem = world.getSystem('RenderSystem')
	renderer = gooRunner.renderer

	# Pre process
	world.processEntityChanges()
	transformSystem._process()
	lightingSystem._process()
	cameraSystem._process()
	boundingSystem._process()

	# Combine
	new goo.EntityCombiner(gooRunner.world).combine()

	# Post process
	world.processEntityChanges()
	transformSystem._process()
	lightingSystem._process()
	cameraSystem._process()
	boundingSystem._process()
	world.getSystem('AnimationSystem')?._process()
	renderSystem._process()

	# Precompile
	renderer.precompileShaders(renderSystem._activeEntities, renderSystem.lights)
	renderer.preloadMaterials(renderSystem._activeEntities)


module.exports = {
	runScene
}
