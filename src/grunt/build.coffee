module.exports = (grunt) ->
	grunt.config.merge
		mf:
			jsOut: 'app.js'
			coffeeIn: 'main.coffee'
			cssOut: 'style.css'
			dependencies: []

		source_types: [
			'coffee'
			'js'
		]

		source_extensions: [
			'.coffee'
			'.js'
		]

		resource_types: [
			'jpg','jpeg','png','gif','svg','ico',
			'json',
			'epub',
			'mp4', 'mp3', 'webm', 'ogg',
			'woff', 'woff2', 'eot', 'ttf'
		]

		static_paths: [
			'resources'
			'res'
			'lib'
		]

		build:
			all:
				tasks: [
					'clean'
					'copy'
					'less'
					'postcss'
					'browserify'
				]

		clean:
			all: ['build']

		browserify:
			vendor:
				files:
					'build/vendor.js': []
				options:
					require: '<%= mf.dependencies %>'

			all:
				files:
					'build/<%= mf.jsOut %>': ['src/<%= mf.coffeeIn %>']
				options:
					external: '<%= mf.dependencies %>'
					transform: ['coffeeify']
					browserifyOptions:
						extensions: '<%= source_extensions %>'

		less:
			all:
				options:
					paths: [
						'src'
						'node_modules'
					]
				files:
					'build/<%= mf.cssOut %>': 'src/{**/,}*.less'
				expand: true

		copy:
			html:
				expand: true
				cwd: 'src'
				src: ['*.html']
				dest: 'build'
			resources:
				expand: true
				cwd: 'src'
				src: ["{,**/}*.{<%= resource_types.join(',') %>}"]
				dest: 'build'
			bower:
				expand: true
				cwd: 'bower_components'
				src: ['{,**/}*.{js,css}']
				dest: 'build/bower_components'

		# cacheBust:
		# 	assets:
		# 		files: [
		# 			expand: true
		# 			src: ['<%= mf.build %>/{,**/}*.html']
		# 		]

		postcss:
			options:
				map: false,
				processors: [
					require('autoprefixer')({browsers: ['last 5 version', 'ie 9']})
				]
			dist:
				src: 'build/<%= mf.cssOut %>'


	grunt.registerMultiTask 'build', 'Build app', ->
		grunt.task.run @data.tasks

