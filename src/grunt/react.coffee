module.exports = (grunt) ->
	grunt.config.merge
		mf:
			coffeeIn: 'main.cjsx'

		browserify:
			all:
				options:
					transform: ['coffee-reactify']

		source_types: grunt.config.get('source_types').concat(['jsx', 'cjsx'])
		source_extensions: grunt.config.get('source_extensions').concat(['.jsx', '.cjsx'])
