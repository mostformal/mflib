module.exports = (grunt) ->

	grunt.config.merge
		mf:
			aws: {}
			cloudflare: {}
			deployPath:
				production: ''
				sandbox: ''
			bucket:
				production: ''
				stage: ''

		deploy:
			prod:
				tasks: [
					'aws_s3:prod'
				]
			production:
				tasks: [
					'aws_s3:prod'
				]
			stage:
				tasks: [
					'aws_s3:stage'
				]
			dev:
				tasks: [
					'aws_s3:stage'
				]

		purge:
			all:
				tasks: [
					'cloudflare'
				]


		aws_s3:
			options:
				accessKeyId: '<%= mf.aws.key %>'
				secretAccessKey: '<%= mf.aws.secret %>'
				region: '<%= mf.aws.region %>'
				access: 'public-read'
				params:
					CacheControl: "max-age=#{1000*60}, public",
					Expires: new Date(Date.now() + 1000*60)
					# ContentEncoding: 'gzip'
				# gzip: true
				# gzipExclude: ['.mp4', '.ogg', '.ogv', '.jpg', '.png']
				# debug: true
				differential: true

			prod:
				options:
					bucket: '<%= mf.bucket.production %>'
				files: [
					expand: true
					cwd: 'build'
					dest: '<%= mf.deployPath.production %>'
					src: ['**']
				]

			stage:
				options:
					bucket: '<%= mf.bucket.stage %>'
				files: [
					expand: true
					cwd: 'build'
					dest: '<%= mf.deployPath.stage %>'
					src: ['**']
				]

		cloudflare:
			options:
				email: '<%= mf.cloudflare.email %>'
				tkn: '<%= mf.cloudflare.apiKey %>'
				z: '<%= mf.cloudflare.domain %>'
				a: 'fpurge_ts'



	grunt.registerMultiTask 'deploy', 'Push to s3', ->
		if grunt.config.get('cloudflare.options.tkn')
			@data.tasks.push('cloudflare')

		grunt.task.run @data.tasks

	grunt.registerMultiTask 'purge', 'Purge cloudflare', ->
		grunt.task.run @data.tasks
