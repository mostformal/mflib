module.exports = (grunt) ->
	grunt.config.merge
		source_types: [
			'js'
			'coffee'
		]

		build:
			all:
				tasks: [
					'clean'
					'copy'
				]

		clean:
			all: ['build']

		copy:
			src:
				expand: true
				cwd: 'src'
				src: [
					"{,**/}*.{<%= source_types.join(',') %>}"
				]
				dest: 'build'


			package:
				expand: true
				cwd: '.'
				src: [
					'package.json'
				]
				dest: 'build'

		deploy:
			all:
				tasks: [
					'build'
					'lambda_package'
					'lambda_deploy'
				]

		lambda_package:
			default:
				options:
					package_folder: ['build']

		lambda_deploy:
			default:
				options:
					region: '<%= mf.region %>'

				arn: 'arn:aws:lambda:<%= mf.region %>:<%= mf.account %>:function:<%= mf.lambdaFunctionName %>'

	grunt.registerMultiTask 'deploy', 'Deploy to aws', ->
		grunt.task.run @data.tasks

	grunt.registerMultiTask 'build', 'Build js bundle', ->
		grunt.task.run @data.tasks

