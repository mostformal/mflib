http = require('http')
https = require('https')
fs = require('fs')
path = require('path')


MAX_CONNECTIONS = 1000
connections = new Set()

start = ({
	cors
	port
	sslPort
	webServer

	enableWebSockets
	onConnect
	onMessage
	onClose
	proxyWrap
}) ->

	port ?= 9000

	app = require('express')()

	if cors
		app.use (req, res, next) ->
			res.header("Access-Control-Allow-Origin", "*")
			res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
			next()

	webServer?(app)

	# Proxywrap is used to work with AWS ELB's ProxyProtocol, which is needed to make websockets work
	# ProxyWrap is broken in latest node
	if proxyWrap
		http = require('proxywrap').proxy(http, strict: proxyWrap == 'strict')


	httpServer = http.createServer(app).listen(port)
	console.log "Server listening on port #{port}"

	if sslPort
		options =
			key: fs.readFileSync(path.resolve(__dirname, 'server.key'))
			cert: fs.readFileSync(path.resolve(__dirname, 'server.crt'))
		httpsServer = https.createServer(options, app).listen(sslPort)
		console.log "HTTPS server listening on port #{sslPort}"

	if enableWebSockets
		WebSocketServer = require('ws').Server
		wsServer = new WebSocketServer
			server: httpServer

		wsServer.on 'connection', (connection) ->
			addConnection(connection, {onConnect, onMessage, onClose})

addConnection = (connection, {onConnect, onMessage, onClose}) ->
	if connections.size > MAX_CONNECTIONS
		console.error('Max connections')
		return

	connections.add(connection)
	console.log "New connection, #{connections.size} connections in total"

	removeConnection = (connection) ->
		if connections.has(connection)
			connections.delete(connection)
			onClose?(connection)

	connection.on 'close', ->
		removeConnection(connection)

	connection.on 'error', (error) ->
		console.error error
		removeConnection(connection)

	connection.on 'disconnect', ->
		removeConnection(connection)

	connection.on 'message', (data) ->
		message = JSON.parse(data)
		onMessage?(message, connection)?.then?((result) ->
			connection.send(JSON.stringify(result)))
		.then(null, (error) ->
			connection.send(JSON.stringify({error: error.toString()})))

	onConnect?(connection)

broadcast = (message) ->
	connections.forEach (connection) ->
		connection.send(JSON.stringify(message))

unwrapQueryParams = (query) ->
	result = {}
	for k, v of query
		if v[..1] == '["' or v[..1] == '{"'
			result[k] = JSON.parse(v)
		else
			result[k] = v
	return result

module.exports = {
	start
	broadcast
	unwrapQueryParams
}