_ = require('lodash')

Env = {}

mobileUserAgents = [
	'Android'
	'BlackBerry'
	'iPhone'
	'iPad'
	'iPod'
	'Opera Mini'
	'IEMobile'
	'Kindle'
	'Silk'
	'Fennec'
	'Tango'
]

re = new RegExp(mobileUserAgents.join('|'), 'i')
Env.isMobile = -> re.test(navigator.userAgent)
Env.isDesktop = -> !Env.isMobile

Env.isWebGLSupported = -> !!window.WebGLRenderingContext

Env.isWebGLDisabled = ->
	###
	Basically copied from the GooJS Renderer
	###
	gl = null
	canvas = document.createElement('canvas')
	contextNames = ["experimental-webgl", "webgl", "moz-webgl", "webkit-3d"]
	for contextName in contextNames
		try
			gl = canvas.getContext(contextName)
			if gl and typeof gl.getParameter == 'function'
				return false
		catch e
			undefined

	return true

Env.isTango = -> /Tango/i.test(navigator.userAgent)

Env.isChromiumVR = -> !!navigator.getVRDevices

Env.isIos = -> /iPhone|iPad|iPod/i.test(navigator.userAgent)


Env.hasVr = ->
	if navigator.getVRDevices?
		navigator.getVRDevices().then (devices) ->
			device = _.find(devices, (d) -> d instanceof PositionSensorVRDevice)
			if device? then return true
	return false

Env.isOpera = -> window.opera? or navigator.userAgent.indexOf(' OPR/') != -1

Env.isFirefox = -> typeof InstallTrigger != 'undefined'
Env.isSafari = -> Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
Env.isChrome = -> window.chrome? and not Env.isOpera
Env.isIE = -> `/*@cc_on!@*/false || !!document.documentMode;`

module.exports = Env

