accessToken = null

loadFBJS = ->
	id = 'facebook-jssdk'
	ref = document.getElementsByTagName('script')[0];
	if document.getElementById(id) then return
	js = document.createElement('script')
	js.id = id
	js.async = true
	js.src = "//connect.facebook.net/en_US/all.js"
	ref.parentNode.insertBefore(js, ref)

initFB = (appId) ->
	FB.init
		appId: appId
		# channelUrl: '//'+ window.location.host + '/channel.html', # Channel File
		status: true
		cookie: true,
		xfbml: true,
		oauth: true


load = (appId) ->
	new Promise (resolve, reject) ->
		window.fbAsyncInit = ->
			initFB(appId)

			FB.Event.subscribe 'auth.statusChange', (auth) ->
				console.log "Auth status changed: ", auth

			FB.getLoginStatus (response) ->
				console.log "Login status is ", response
				if response.status == 'connected'
					resolve(response.authResponse.accessToken)
				else
					resolve()

		loadFBJS()

login = ->
	if accessToken then return Promise.resolve(accessToken)

	new Promise (resolve, reject) ->
		console.log "Calling fb-login"
		FB.login (response) ->
			console.log "fb-login returned ", response

			if response.authResponse
				resolve(response.authResponse.accessToken)
			else
				reject(response)

module.exports = {
	load
	login
}