_ = require('lodash')
AWS = require('aws-sdk')
{wrapApi} = require('./ApiWrapper.coffee')
fs = wrapApi(require('fs'))
s3 = wrapApi(new AWS.S3())
eventTarget = require('./EventTarget.coffee')()


getCredentialsFromFile = (path) ->
	fs.readFile(path)
	.then (contents) ->
		awsConfig = JSON.parse(contents)
		return {
			accessKeyId: awsConfig.key
			secretAccessKey: awsConfig.secret
			region: awsConfig.region
			sslEnabled: true
		}

getCredentialsFromEnv = ->
	new Promise (resolve, reject) ->
		if process.env.AWS_KEY? and process.env.AWS_SECRET?
			console.log "Taking AWS credentials from env"
			resolve
				accessKeyId: process.env.AWS_KEY
				secretAccessKey: process.env.AWS_SECRET
				region: process.env.AWS_REGION
		else
			reject(new Error("AWS_KEY or AWS_SECRET missing from env"))

getCredentialsFromS3 = (bucket, key = 'aws.json') ->
	s3.getObject({Bucket: bucket, Key: key})
	.then (data) ->
		env = JSON.parse(data.Body.toString('utf8'))
		data =
			accessKeyId: env.key
			secretAccessKey: env.secret
			region: env.region

cache = {}
getCredentials = ({file, bucket, key, nocache}) ->
	cacheKey = "#{file}/#{bucket}/#{key}"
	if cache[cacheKey] and not nocache
		return Promise.resolve(cache[cacheKey])

	error = null
	getCredentialsFromEnv()
	.then null, ->
		if file then getCredentialsFromFile(file) else Promise.reject()
	.then null, (e) ->
		return null
	.then (credentials) ->
		if not bucket?
			if credentials then return credentials
			throw new Error("Could not get credentials from env or file #{file}")

		if credentials?
			AWS.config = credentials
			s3 = wrapApi(new AWS.S3())
		getCredentialsFromS3(bucket, key)
	.then (credentials) ->
		cache[cacheKey] = credentials
		eventTarget.dispatchEvent('newCredentials', credentials)
		return credentials

# ------------------------------------------------------------------------

sign = (credentials, {region, bucket, keys}) ->
	if not credentials?.accessKeyId?
		throw new Error("No valid credentials")

	signedForms = signForms {
		accessKeyId: credentials.accessKeyId
		secretAccessKey: credentials.secretAccessKey
		region
		bucket
		keys
	}

AwsS3Form = require('aws-s3-form')

signForms = ({accessKeyId, secretAccessKey, region, bucket, keys}) ->
	formGen = new AwsS3Form({accessKeyId, secretAccessKey, region, bucket})
	result = {}
	if not _.isArray(keys) then keys = [keys]
	for key in keys
		result[key] = formGen.create(key).fields

	return result

module.exports = _.extend eventTarget, {
	getCredentials
	sign
}