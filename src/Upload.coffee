_ = require('lodash')
Ajax = require('./Ajax.coffee')
{arrayBufferFromFile} = require('./BinUtils.coffee')

upload = ({signUrl, region, bucket, folder, files, onProgress}) ->
	fileMap = {}
	prefix = if folder? then "#{folder}/" else ""
	for file in files
		key = "#{prefix}#{file.name}"
		fileMap[key] = file

	progress = {}

	_upload = (key, form) ->
		progress[key] = 0
		Ajax.post
			url: "//#{bucket}.s3.amazonaws.com"
			data: form
			onProgress: (_progress) ->
				if not onProgress? then return
				progress[key] = _progress
				totalProgress = _.sum(_.values(progress))/_.keys(progress).length
				if isNaN(totalProgress) then totalProgress = 0
				onProgress(totalProgress)


	getSignedForm(signUrl, {region, bucket, keys: _.keys(fileMap)})
	.then (signedForms) ->
		Promise.all(for key, file of fileMap
			form = signedForms[key]
			if not form?
				throw new Error("No signed form for #{key}")
			form.file = file
			_upload(key, form)
		)
	.then ->
		{folder}

download = (name, file) ->
	url = URL.createObjectURL(file)
	link = document.createElement('a')
	link.download = name
	link.href = url
	document.body.appendChild(link)
	link.click()
	setTimeout ->
		document.body.removeChild(link)

module.exports = {
	upload
	download
}

# ------------------------------------------------

getSignedForm = (signUrl, {region, bucket, keys}) ->
	if not _.isArray(keys) then keys = [keys]
	Ajax.get(url: "#{signUrl}", data: {region, bucket, keys})

# createZip = (name, files) ->
# 	zip = new JSZip().folder(name)
# 	Promise.all(arrayBufferFromFile(file) for file in files)
# 	.then (buffers) ->
# 		for buffer, idx in buffers
# 			zip.file(files[idx].name, buffer)
# 		return zip.generate(type: 'blob')
