Server = require('../src/Server.coffee')

# Server.start
# 	cors: true
# 	port: 8000
# 	webServer: (app) ->
# 		app.use (request, response) ->
# 			response.statusCode = 200
# 			response.end('Hello world')


{getCredentials} = require('../src/AwsCredentials.coffee')

getCredentials(file: './aws.json', bucket: 'cancerchoir-config')
.then (c) ->
	console.log c
, (e) ->
	console.log e